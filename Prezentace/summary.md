# O nás
 * na začátku celé prezentace
 * Jindra o tom má připravenou prezentaci
 * nudná ⇒ proložit koťátky!!!

# Metodika
  * přednáška bude anachronická – historie QM je velmi zajímavá, zároveň je
    ale plná nepřesných myšlenek a objevů, které byly učiněny až příliš pozdě, na to jak důležité jsou
  * zároveň jsem ale přesvědčen, že je mnohem víc odměňující novou fyzikální teorii postupně "objevit" – po kouscích postupovat od známého k neznámému – než ji dostat naservírovanou na podnosu. myslím si to z několika důvodů:
    1. objevování je zábava!
    2. tenhle přístup studujícího podporuje v kritickém přemýšlení – naším cílem není přijmout nějakou teorii jako dogmatickou pravdu, naším cílem je podívat se na výsledky experimentů a na základě nich:
          1. usoudit jaké teorie vůbec připadají v úvahu, a jaké jsou rozhodně mimo hru
          2. najít nějakou jednu teorii, ve které se pohodlně pracuje a dává nám dobré predikce
    3. navíc věřím, že tenhle přístup nám dá mnohem hlubší porozumění, než kdybychom se zkrátka naučili používat nějaký existující model, nehledě na to, jak přesné predikce dává

  * poznámka bokem: lidem, kteří fyziku nestudovali, by se mohlo zdát trochu zvláštní, že rovnou mluvím rovnou o spoustě různých "možných" teorií/modelů. "copak to není tak, že nejdřív byla klasická (newtonovská) fyzika, a my jsme potom zjistili, že ta je špatně, a teď máme kvantovou mechaniku a ta je správně?" → není to tak jednoduché! když máme jakoukoliv fyzikální teorii, je relativně snadné udělat v ní drobnou změnu, která bude pořád v souladu se všemi dosavadními experimenty. takže často velmi dobře víme, jak teorie vypadat *nemůže*, protože je to v rozporu s pozorováním


# Kontext
 * Chci, abyste se vcítili do fyzika na začátku 20. století
    - "newtonovská" mechanika (tj. zákon akce a reakce – popsat!!, zrychlení v závislosti na síle atd.) je prozkoumaná do nejdrobnějších detailů
    - ekvivalentní popis:
      - Jan má dceru Annu a bratra Václava
      - "Anna je neteř Václava"
      - "Václav je strýc Anny"
    - nedávno byla objevena dobrá teorie elektřiny a magnetizmu
       - aby se s nimi dobře počítalo, začali jsme používat něco, čemu se říká "vektory"
       - zdá se, že tahle teorie nefunguje až tak dobře pro rychle se pohybující pozorovatele, ale nějaký chytrák z Rakouska-Uherska už na tom prý pracuje

 * Elektromagnetizmus:
   - existují částice (malé kuličky, třeba částice prachu),
     které mají různý "elektrický náboj" (číslo)
       - částice, které mají opačný náboj se přitahují
       - částice, které mají stejný náboj se odpuzují
   - existují magnety, které mají severní a jižní pól
       - stejné póly se odpuzují, opačné se přitahují
   - elektřina a magnetismus se navzájem ovlivňují
        - když kroužím nabitou částicí, magnety se chovají, jako kdyby tam byl magnet
        - když k letící nabité částici přiložím magnet, začne kroužit
   - zdá se, že žádné magnetické částice neexistují
      -  → všechny magnety jsou vytvořeny kroužícími částicemi
   - magnety a elektricky nabité věci se ovlivňují "na dálku"
      -  → neděje se to okamžitě, ve skutečnosti existují tzv. pole (elektrické a magnetické), změny na nich se šíří jako vlny na jezeře, a to přesně danou rychlostí – rychlostí světla
   - světlo, rádiové vlny, rentgenové záření – to všechno jsou druhy tzv. elektromagnetického záření, které vzniká při pohybu magnetů/elektricky nabitých částic
      - světlo se může vlnit jinak svižně (frekvence / vlnová délka / barva), jinak mohutně (amplituda / intenzita) a jiným směrem (polarizace)

 * Nejlepší představa atomu je zatím:
    - jádro je hromada kladně nabitých protonů a nenabitých neutronů
    - kolem nich lítají záporně nabité elektrony
        - neodletí pryč, protože je přitahuje jádro
        - nespadnou dolů, protože odstředivá síla
        - lítají jako měsíc kolem země nebo země kolem slunce
        - → tzv. orbitální teorie
        - čím dál je elektron od atomu, tím vyšší má (potenciální) energii
           - → neintuitivní, vysvětlit, zjistit!!!

    * Hvězdy, planety a měsíce lítají po různých orbitách a navzájem se různě ovlivňují. Dokážeme nějak zkoumat orbity elektronů kolem jader?

    * Ano, dokážeme! Vzpomínáte, jak pohybující se částice vyzařují elektromagnetické záření? To nám může poskytnout všechny informace o pohybu elektronů na orbitách
      kolem jader!
        * anachronické přirovnání: gravitační vlny – gravitační interakce všech hmotných předmětů "rozvlní prostor" a s dostatečně citlivými přístroji bychom dokázali rekonstruovat, jak přesně se pohybovaly
        * stejně tak teorie elektromagnetismu předvídá, že elektrony obíhající kolem středu atomů by měly vyzařovat elektromagnetické záření – tentokrát mnohem silnější
    * Skutečné pozorování:
        * podivnost: atomy v klidu nevyzařují žádné EM záření, a když je vybudíme, vyzařují jen záření o konkrétních, pevně daných vlnových délkách/frekvencích/barvách
        * netušíme, jak to vysvětlit! zkusíme si tipnout, jak by to *zhruba* mohlo být:
        * elektrony se nemůžou pohybovat jakkoliv, ale mají pevně dané "poličky", na kterých sedí, a jinde být nesmí
        * každá "polička" má nějakou energii
        * dokud jsou na "poličkách", nic nevyzařují
        * když se přesunou mezi "poličkami", přijmou nebo vyzáří rozdíl energií

    * tahle teorie funguje!
        * = dobře predikuje pozorované vlnové délky
        * ≠ víme, co přesně se v atomu děje (a abych vás zbytečně nenapínal – to se přesně v průběhu této přednášky nedozvíme, vydáme se ale na cestu, která k odpovědi vede, a v případě zájmu můžeme např. zorganizovat přednášku, kde se k tomu dostaneme)
      * můžeme projít všechny známé prvky a sepsat si, jaké "poličky" mají, zajímavé pozorování:
        * do každého orbitalu se vejdou dva elektrony
        * orbitaly můžou být různě vysoko (→ roste jejich energie)a různě prostorově orientované
        * magnetické štěpení → je elektron malý magnetek?

    * PAUZA → SUDOKU!


# "Magnetická vlastnost elektronu"
  * chceme prozkoumat, jestli elektron je skutečně malý magnetek
  * jak ukázat, že je něco magnet? to je jednoduché – stačí ho dát do magnetického pole!
    * není to tak jednoduché:
      * v rovnoměrném magnetickém poli se magnet jen otáčí, ale nezrychluje
      * → použijeme nerovnoměrné pole (např jen jeden magnet)
      * nabitá částice v magnetickém poli krouží
      * → musíme proto použít neutrální částici
  
  * atom stříbra nebo draslíku
    * každý mají všechny elektrony krásně spárované – kromě jednoho volného

![](https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Electron_shell_047_Silver.svg/223px-Electron_shell_047_Silver.svg.png)
![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Electron_shell_019_Potassium.svg/223px-Electron_shell_019_Potassium.svg.png)

  * pokud jste skeptici, má to jednu drobnou nevýhodu – musíte mi *věřit*, že jsou elektrony takhle uspořádané, teorii, která by to přesně předpovídala totiž zatím nemáme
  * naštěstí proton & neutron mají taky magnetický moment, asi tisíckrát slabší, takže skeptici si místo stříbra nebo draslíku můžou představovat neutrony


  * vezmeme pec se stříbrem / draslíkem / zdroj neutronů
  * přidáme kolimátor, magnet, stínítko
  * můžeme testovat!

  * ale pozor: lidský mozek je velmi dobrý ve vysvětlování pozorovaných jevů pomocí našich přesvědčení o tom, jak svět funguje
    * když máme nějakou představu, že něco funguje takhle a takhle, a pozorujeme nějaký jev, náš mozek se bude snažit použít naše současné porozumění, aby vysvětlil, proč se to stalo – a to i v případě, že pozorování ve skutečnosti *není* v souladu s naší představou světa
    * TODO vymyslet příklad
    * proto je důležité stanovit *hypotézu*, co se stane!!!
    * pojďme si tedy říci, jaké výsledky očekáváme

  * hypotézy:
    * v případě, že atom není magnetický:
      * poletí přímo dopředu, bude se chovat stejně se zapnutým i vypnutým magnetickým polem
    * v případě, že atom je magnetický:
      * magnetické pole jednak přitahuje celý atom, a jednak se ho snaží otočit
      * elektron je "jednoduché posouvat, těžké otáčet":
        * atom odkloněn jiným směrem, ale nestihne se otočit
        * náhodný směr → normální distribuce

      * elektron je "jednoduché otáčet, těžké posouvat":
        * bez tření:
          * atom osciluje
          * náhodný směr → normální distribuce

        * se třením:
          * atom se zarovná shodně s polem
          * náhodný směr → normální distribuce
  
  * výsledek:
    * dva body!!!
    * intenzita 50% : 50%
    * → atom se velmi rychle zarovná shodně nebo opačně s magnetickým polem

  * tohle podivné chování si zaslouží prozkoumat detailněji!
    * zkusíme přidat jeden další magnet
    * tam, kde byl jeden z bodů, přidáme štěrbinu a necháme paprsek proletět k dalšímu magnetu
    * druhý magnet lze otočit o libovolný úhel $\alpha$
    * za druhý magnet přidáme stínítko
    * měříme, jaká intenzita je na stínítku v závislosti na $\alpha$

    * hypotézy:
      * magnet se natočí tím pólem, kterým je blíž → intenzita 100 % : 0 %
      * ???

    * výsledek:
      * $(\cos^2 \tfrac{\alpha}{2})$ : $(1 - \cos^2 \tfrac{\alpha}{2})$
  
    * vysvětlení: ???!?!
    * když člověk neví, často se vyplatí udělat cimrmanovský "úkrok stranou"
    * proto se teď podíváme na něco *úplně jiného*!

# Vektory
  1. vektor jako šipka
  2. vektor jako $n$-tice čísel
  3. délka vektoru (Pythagorova věta), jednotkový vektor
  4. délka vektoru v nějakém směru (projekce, geometricky)
  5. vektor otočený nějakým směrem ($\sin$, $\cos$)
  6. délka vektoru ve směru jiného (jednotkového) vektoru (skalární součin)

# Polarizace
  * TODO
 

# Sekvenční SG s více kroky
  * TODO
  * atom "zapomene" v jakém byl stavu

# Interpretace
  * TODO
  *


# Provázaný stav
 * TODO
